import {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull
} from 'graphql';

import Db from './db';
import { GraphQLBoolean, GraphQLID, GraphQLFloat } from 'graphql/type/scalars';

const Admin = new GraphQLObjectType({
    name: 'admin',
    description: 'This is admins',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(admins){
                    return admins.id;
                }
            },
            mobno: {
                type: GraphQLString,
                resolve(admins){
                    return admins.mobno;
                }
            },
            username: {
                type: GraphQLString,
                resolve(admins){
                    return admins.username;
                }
            },
            password: {
                type: GraphQLString,
                resolve(admins){
                    return admins.password;
                }
            },
            region: {
                type: GraphQLString,
                resolve(admins){
                    return admins.region;
                }
            },
            hh: {
                type: GraphQLString,
                resolve(admins){
                    return admins.hh;
                }
            },
            email: {
                type: GraphQLString,
                resolve(admins){
                    return admins.email;
                }
            },
        }
    }
});

const Employee = new GraphQLObjectType({
    name: 'employee',
    description: 'This is employees',
    fields: () => {
        return {
            id: {
                type: GraphQLString,
                resolve(employees){
                    return employees.id;
                }
            },
            empid: {
                type: GraphQLString,
                resolve(employees){
                    return employees.empid;
                }
            },
            empname: {
                type: GraphQLString,
                resolve(employees){
                    return employees.ename;
                }
            },
            empmobile: {
                type: GraphQLString,
                resolve(employees){
                    return employees.emobno;
                }
            },
            empemailid: {
                type: GraphQLString,
                resolve(employees){
                    return employees.empemail;
                }
            },
            designation: {
                type: GraphQLString,
                resolve(employees){
                    return employees.designation;
                }
            },
        }
    }
});

const Booking = new GraphQLObjectType({
    name: 'Booking',
    description: 'This is bookings',
    fields: () => {
        return {
            id: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.id;
                }
            },
            empid: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.empid;
                }
            },
            empname: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.ename;
                }
            },
            empmobile: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.emobno;
                }
            },
            empemailid: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.empemail;
                }
            },
            designation: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.designation;
                }
            },
            hhid: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.hhid;
                }
            },
            fdate: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.fdate;
                }
            },
            tdate: {
                type: GraphQLString,
                resolve(bookings){
                    return bookings.tdate;
                }
            }
        }
    }
});

const Region = new GraphQLObjectType({
    name: 'region',
    description: 'This is regions',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(regions){
                    return regions.id;
                }
            },
            regionid: {
                type: GraphQLInt,
                resolve(regions){
                    return regions.regionid;
                }
            },
            regionname: {
                type: GraphQLString,
                resolve(regions){
                    return regions.regionname;
                }
            },
            holidayhome: {
                type: new GraphQLList(HolidayHome),
                resolve(regions){
                    return regions.getHolidayhomes();
                }
            },
        }
    }
});

const Image = new GraphQLObjectType({
    name: 'image',
    description: 'This is images',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(images){
                    return images.id;
                }
            },
            imageurl: {
                type: GraphQLString,
                resolve(images){
                    return images.imageurl;
                }
            }
        }
    }
});

const Comment = new GraphQLObjectType({
    name: 'comment',
    description: 'This is comments',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(comments){
                    return comments.id;
                }
            },
            commenttag: {
                type: GraphQLString,
                resolve(comments){
                    return comments.commenttag;
                }
            },
            commententity: {
                type: GraphQLString,
                resolve(comments){
                    return comments.commententity;
                }
            },
            commentdetail: {
                type: GraphQLString,
                resolve(comments){
                    return comments.commentdetail;
                }
            }
        }
    }
});

const Station = new GraphQLObjectType({
    name: 'station',
    description: 'This is stations',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(stations){
                    return stations.id;
                }
            },
            stationcode: {
                type: GraphQLInt,
                resolve(stations){
                    return stations.stationcode;
                }
            },
            stationname: {
                type: GraphQLString,
                resolve(stations){
                    return stations.stationname;
                }
            },
            room: {
                type: new GraphQLList(Room),
                resolve(stations){
                    return stations.getRoom();
                }
            }
        }
    }
});

const Room = new GraphQLObjectType({
    name: 'room',
    description: 'This is rooms',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(rooms){
                    return rooms.id;
                }
            },
            roomno: {
                type: GraphQLString,
                resolve(rooms){
                    return rooms.roomno;
                }
            },
            bedtype: {
                type: GraphQLString,
                resolve(rooms){
                    return rooms.bedtype;
                }
            },
            roomtype: {
                type: GraphQLString,
                resolve(rooms){
                    return rooms.roomtype;
                }
            },
            tariff: {
                type: GraphQLFloat,
                resolve(rooms){
                    return rooms.tariff;
                }
            },
            comment: {
                type: new GraphQLList(Comment),
                resolve(rooms){
                    return rooms.getComments();
                }
            },
            available: {
                type: GraphQLBoolean,
                resolve(rooms){
                    return rooms.available;
                }
            },
        }
    }
});

const Suite = new GraphQLObjectType({
    name: 'suite',
    description: 'This is suites',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(suites){
                    return suites.id;
                }
            },
            suitename: {
                type: GraphQLString,
                resolve(suites){
                    return suites.suitename;
                }
            },
            suitetype: {
                type: GraphQLString,
                resolve(suites){
                    return suites.suitetype;
                }
            },
            suiterating: {
                type: GraphQLString,
                resolve(suites){
                    return suites.suiterating;
                }
            },
            suiterate: {
                type: GraphQLString,
                resolve(suites){
                    return suites.suiterate;
                }
            },
            suiteavail: {
                type: GraphQLString,
                resolve(suites){
                    return suites.suiteavail;
                }
            },
            bedtype: {
                type: GraphQLString,
                resolve(suites){
                    return suites.bedtype;
                }
            },
            comment: {
                type: new GraphQLList(Comment),
                resolve(suites){
                    return suites.getComments();
                }
            },
            image: {
                type: new GraphQLList(Image),
                resolve(suites){
                    return suites.getImages();
                }
            },
            holidayhomeId: {
                type: GraphQLInt,
                resolve(suites){
                    return suites.holidayhomeId;
                }
            },
        }
    }
});

const HolidayHome = new GraphQLObjectType({
    name: 'HolidayHome',
    description: 'This is holiday homes',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                resolve(holidayhomes){
                    return holidayhomes.id;
                }
            },
            hhname: {
                type: GraphQLString,
                resolve(holidayhomes){
                    return holidayhomes.hhname;
                }
            },
            hhlocation: {
                type: GraphQLString,
                resolve(holidayhomes){
                    return holidayhomes.hhlocation;
                }
            },
            hhadetails: {
                type: GraphQLString,
                resolve(holidayhomes){
                    return holidayhomes.hhadetails;
                }
            },
            hhacontact: {
                type: GraphQLString,
                resolve(holidayhomes){
                    return holidayhomes.hhacontact;
                }
            },
            hhdate: {
                type: GraphQLString,
                resolve(holidayhomes){
                    return holidayhomes.hhdate;
                }
            },
            hhavail: {
                type: GraphQLInt,
                resolve(holidayhomes){
                    return holidayhomes.hhavail;
                }
            },
            hhremavail: {
                type: GraphQLInt,
                resolve(holidayhomes){
                    return holidayhomes.hhremavail;
                }
            },
            regionid: {
                type: GraphQLInt,
                resolve(holidayhomes){
                    return holidayhomes.regionId;
                }
            },
            image: {
                type: new GraphQLList(Image),
                resolve(holidayhomes){
                    return holidayhomes.getImages();
                }
            },
            suite: {
                type: new GraphQLList(Suite),
                resolve(holidayhomes){
                    return holidayhomes.getSuites();
                }
            },
        }
    }
});



const Query = new GraphQLObjectType({
    name: 'Query',
    description: 'This is Query',
    fields: ()=>{
        return{
            
            admin: {
                type: new GraphQLList(Admin),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    mobno: {
                        type: GraphQLString
                    },
                    email: {
                        type: GraphQLString
                    },
                    region: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.admins.findAll({where:args});
                }
            },
            employee: {
                type: new GraphQLList(Employee),
                args: {
                    empid: {
                        type: GraphQLString
                    },
                    empmobile: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.employees.findAll({where:args});
                }
            },
            booking: {
                type: new GraphQLList(Booking),
                args: {
                    empid: {
                        type: GraphQLString
                    },
                    empmobile: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.bookings.findAll({where:args});
                }
            },
            region: {
                type: new GraphQLList(Region),
                args: {
                    regionid: {
                        type: GraphQLInt
                    },
                    regionname: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.regions.findAll({where:args});
                }
            },
            holidayhome: {
                type: new GraphQLList(HolidayHome),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    hhlocation: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.holidayhomes.findAll({where:args});
                }
            },
            suite: {
                type: new GraphQLList(Suite),
                args: {
                    holidayhomeId: {
                        type: GraphQLInt
                    },
                    suitetype: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.suites.findAll({where:args});
                }
            },
            station: {
                type: new GraphQLList(Station),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    stationcode: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.stations.findAll({where:args});
                }
            },
            comment: {
                type: new GraphQLList(Comment),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    commenttag: {
                        type: GraphQLString
                    }
                },
                resolve(root,args) {
                    return Db.models.comments.findAll({where:args});
                }
            },
            room: {
                type: new GraphQLList(Room),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    roomno: {
                        type: GraphQLInt
                    },
                    bedtype: {
                        type: GraphQLString
                    },
                    roomtype: {
                        type: GraphQLString
                    },
                    available: {
                        type: GraphQLBoolean
                    },
                },
                resolve(root,args) {
                    return Db.models.rooms.findAll({where:args});
                }
            },
            
    }
}
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    description: 'create some stuff',
    fields() {
        return{
            
            addAdmin:{
                type: Admin,
                args: {
                    
                    username: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    password: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    mobno: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    email: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    region: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hh: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(_,args){
                    return Db.models.admins.create({
                        username: args.username,
                        password: args.password,
                        mobno: args.mobno,
                        email: args.email,
                        region: args.region,
                        hh: args.hh,
                    });
                }
            },
            addRegion:{
                type: Region,
                args: {
                    
                    regionid: {
                        type: new GraphQLNonNull(GraphQLID)
                    },
                    regionname: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(_,args){
                    return Db.models.regions.create({
                        regionid: args.regionid,
                        regionname: args.regionname,
                    });
                }
            },
            addComment:{
                type: Comment,
                args: {
                    
                    commenttag: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    commententity: {
                        type: GraphQLString
                    },
                    commentdetail: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    suiteid: {
                        type: GraphQLString
                    },
                    roomid: {
                        type: GraphQLString
                    },
                },
                resolve(_,args){
                    return Db.models.comments.create({
                        commenttag: args.commenttag,
                        commentdetail: args.commentdetail,
                        commententity: args.commententity,
                        suiteId: args.suiteid,
                        roomId: args.roomid                        
                    });
                }
            },
            addHolidayHome:{
                type: HolidayHome,
                args: {
                    
                    hhname: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hhlocation: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hhadetails: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hhacontact: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hhavail: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    hhdate: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hhremavail: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    regionid: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.holidayhomes.create({
                        hhname: args.hhname,
                        hhlocation: args.hhlocation,
                        hhadetails: args.hhadetails,
                        hhacontact: args.hhacontact,
                        hhdate: args.hhdate,
                        hhavail: args.hhavail,
                        hhremavail: args.hhremavail,
                        regionId: args.regionid
                    });
                }
            },
            deleteHH:{
                type: HolidayHome,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.holidayhomes.destroy({where:args});
                }
            },
            deleteHHImage:{
                type: Image,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.images.destroy({where:args});
                }
            },
            addHHImage:{
                type: Image,
            
                args: {
                    
                    imageurl: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    holidayhomeid: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.images.create({
                        imageurl: args.imageurl,
                        holidayhomeId: args.holidayhomeid,
                    });
                }
            },
            addSuiteImage:{
                type: Image,
                args: {
                    
                    imageurl: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    suiteid: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.images.create({
                        imageurl: args.imageurl,
                        suiteId: args.suiteid,
                    });
                }
            },
            addSuite:{
                type: Suite,
                args: {
                    
                    suitename: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    suitetype: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    suiterating: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    suiterate: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    suiteavail: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    bedtype: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    holidayhomeid: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.suites.create({
                        suitename: args.suitename,
                        suitetype: args.suitetype,
                        suiterating: args.suiterating,
                        suiterate: args.suiterate,
                        suiteavail: args.suiteavail,
                        bedtype: args.bedtype,
                        holidayhomeId: args.holidayhomeid
                    });
                }
            },
            addEmployee:{
                type: Employee,
                args: {
                    
                    empname: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    empid: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    emonno: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    empemail: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    designation: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(_,args){
                    return Db.models.employees.create({
                        ename: args.empname,
                        empid: args.empid,
                        emobno: args.emonno,
                        empemail: args.empemail,
                        designation: args.designation,
                    });
                }
            },
            addBooking:{
                type: Booking,
                args: {
                    
                    empname: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    empid: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    emonno: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    empemail: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    designation: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    hhid: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    fdate: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    tdate: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(_,args){
                    return Db.models.bookings.create({
                        ename: args.empname,
                        empid: args.empid,
                        emobno: args.emonno,
                        empemail: args.empemail,
                        designation: args.designation,
                        hhid: args.hhid,
                        fdate: args.fdate,
                        tdate: args.tdate,
                    });
                }
            },
            addRoom:{
                type: Room,
                args: {
                    
                    roomno: {
                        type: new GraphQLNonNull(GraphQLInt)
                    },
                    bedtype: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    roomtype: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    tariff: {
                        type: new GraphQLNonNull(GraphQLFloat)
                    },
                    available: {
                        type: new GraphQLNonNull(GraphQLBoolean)
                    },
                    stationId: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_,args){
                    return Db.models.rooms.create({
                        roomno: args.roomno,
                        bedtype: args.bedtype,
                        roomtype: args.roomtype,
                        tariff: args.tariff,
                        available: args.available,
                        stationId: args.holidayhomeid
                    });
                }
            },
            addStation:{
                type: Station,
                args: {
                    
                    stationcode: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    stationname: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(_,args){
                    return Db.models.stations.create({
                        stationcode: args.stationcode,
                        stationname: args.stationname,
                    });
                }
            },
        }
        
    }
});

const Schema = new GraphQLSchema({
    query: Query,
    mutation: Mutation
});

export default Schema;