import Sequelize from 'sequelize';
import _ from 'lodash';
import Faker from 'faker';
import { F_OK } from 'constants';

//Deployment

const  Conn = new Sequelize('postgres://nssbkuyubfihze:fbe4c27c9df5a6c7981b495a92db029ba0f8cc4ebaf936fcbbee4b14bc777bfb@ec2-54-197-254-189.compute-1.amazonaws.com:5432/dcdo4taic39oal');

// Development

// const Conn = new Sequelize(
//     'mytestdb',
//     'postgres',
//     'jit',
//     {
//         dialect: 'postgres',
//         host: 'localhost'
//     }
// );




const Admins = Conn.define('admins',{
    username: {
        type: Sequelize.STRING,
        allowNull: true
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    mobno: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true,
        }
    },
    region: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hh: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Employees = Conn.define('employees',{
    empid: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ename: {
        type: Sequelize.STRING,
        allowNull: false
    },
    emobno: {
        type: Sequelize.STRING,
        allowNull: false
    },
    empemail: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true,
        }
    },
    designation: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Bookings = Conn.define('bookings',{
    empid: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    ename: {
        type: Sequelize.STRING,
        allowNull: false
    },
    emobno: {
        type: Sequelize.STRING,
        allowNull: false
    },
    empemail: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true,
        }
    },
    designation: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hhid: {
        type: Sequelize.STRING,
        allowNull:false
    },
    fdate: {
        type: Sequelize.STRING,
        allowNull:false
    },
    tdate: {
        type: Sequelize.STRING,
        allowNull:false
    }
});



const Regions = Conn.define('regions',{
    regionid: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    regionname: {
        type: Sequelize.STRING,
        allowNull: false
    }

});

const HolidayHomes = Conn.define('holidayhomes',{
    hhname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hhlocation: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hhadetails: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hhacontact: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hhdate: {
        type: Sequelize.STRING,
        allowNull: false
    },
    hhavail: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    hhremavail: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
});

const Suites = Conn.define('suites',{
    suitename: {
        type: Sequelize.STRING,
        allowNull: false
    },
    suitetype: {
        type: Sequelize.STRING,
        allowNull: false
    },
    suiterating: {
        type: Sequelize.STRING,
        allowNull: false
    },
    suiterate: {
        type: Sequelize.STRING,
        allowNull: false
    },
    suiteavail: {
        type: Sequelize.STRING,
        defaultValue: true
    },
    bedtype: {
        type: Sequelize.STRING,
        allowNull: false
    },

});

const Images = Conn.define('images',{
    imageurl: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Amentities = Conn.define('amentities',{
    amname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    amavail: {
        type: Sequelize.STRING,
        defaultValue: false
    },
});

const Stations = Conn.define('stations',{
    stationcode: {
        type: Sequelize.STRING,
        allowNull: false
    },
    stationname: {
        type: Sequelize.STRING,
        allowNull: false
    },
});

const Rooms = Conn.define('rooms',{
    roomno: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    bedtype: {
        type: Sequelize.STRING,
        allowNull: false
    },
    roomtype: {
        type: Sequelize.STRING,
        allowNull: false
    },
    tariff: {
        type: Sequelize.FLOAT,
        allowNull: false
    },
    available: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
    },
});

const Comments = Conn.define('comments',{
    commenttag: {
        type: Sequelize.STRING,
        allowNull: true
    },
    commententity: {
        type: Sequelize.STRING,
        allowNull: true
    },
    commentdetail: {
        type: Sequelize.TEXT,
        allowNull: false
    },
});

Suites.hasMany(Images);
Suites.hasMany(Amentities);
Suites.hasMany(Comments);
Comments.belongsTo(Suites);
Stations.hasHook(Rooms);
Rooms.belongsTo(Stations);
Images.belongsTo(Suites);
Amentities.belongsTo(Suites);
HolidayHomes.hasMany(Images);
Images.belongsTo(HolidayHomes);
HolidayHomes.hasMany(Suites);
Suites.belongsTo(HolidayHomes);
Regions.hasMany(HolidayHomes);
HolidayHomes.belongsTo(Regions);
Suites.hasMany(Comments);
Comments.belongsTo(Suites);



Conn.sync();
// _.times(10,()=>{
//     return Regions.create({
//         regionid:Faker.random.number(10),
//         regionname: Faker.random.word()
//     });
// });
// _.times(20,()=>{
//     return HolidayHomes.create({
//         hhname: Faker.company.companyName(),
//         hhlocation: Faker.address.streetAddress(),
//         hhacontact:Faker.phone.phoneNumber(),
//         hhdate:Faker.name.title(),
//         hhadetails:Faker.address.state(),
//         hhavail:Faker.random.number(),
//         hhremavail:Faker.random.number(),
//         regionId: Faker.random.number({min:1,max:10})
//     });
// });
// _.times(10,()=>{
//     return Suites.create({
//             suitename:Faker.company.companyName(),
//            suiterate:Faker.commerce.price(),
//            suiterating:Faker.random.number(5),
//            suiteavail:Faker.random.number(5),
//            suitetype: "A",
//            bedtype:Faker.random.word(),
//            holidayhomeId:Faker.random.number(20)
//     });
// });
// _.times(10,()=>{
//     return Suites.create({
        
//             suitename:Faker.company.companyName(),
//            suiterate:Faker.commerce.price(),
//            suiterating:Faker.random.number(5),
//            suiteavail:Faker.random.number(5),
//            suitetype: "B",
//            bedtype:Faker.random.word(),
//            holidayhomeId:Faker.random.number(20)
//     });
// });
// _.times(10,()=>{
//     return Suites.create({
       
//             suitename:Faker.company.companyName(),
//            suiterate:Faker.commerce.price(), 
//            suiterating:Faker.random.number(5),
//            suiteavail:Faker.random.number(5),
//            suitetype: "C",
//            bedtype:Faker.random.word(),
//            holidayhomeId:Faker.random.number(20)
           
//     });
// });
// _.times(30,()=>{
//     return Images.create({
//         imageurl:Faker.image.imageUrl(),
//         suiteId:Faker.random.number(25),
//   });
// });

// {force: true}
export default Conn;